import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { ArticleService } from './article.service';
import { AppComponent }  from './app.component';
import { ArticleDetailComponent } from './article-detail.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    ArticleDetailComponent
  ],
   providers: [ ArticleService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

