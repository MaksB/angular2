import { Component, Input } from '@angular/core';
import { Article } from './article';


@Component({
  selector: 'my-article-detail',
  template: `
		<div *ngIf="article">
		  <h1>Id: {{article.id}}</h1>
		  <h2>Title: {{article.title}}</h2>
		  <h2>Text: {{article.text}}</h2>
			<div>
				<label>Title: </label>
				<input [(ngModel)]="article.title" placeholder="title">
				<p>
					<label>Text: </label>
					<textarea [(ngModel)]="article.text" placeholder="title" cols="20" rows="3"></textarea>
				</p>
			</div>
		</div>
`
})
export class ArticleDetailComponent {
	 @Input()
	 article: Article;
	  
}	

