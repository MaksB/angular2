import { Component } from '@angular/core';

import { Article } from './article';
import { ArticleService } from './article.service';

@Component({
    selector: 'my-app',
    template: `
    <h2>Articles</h2>
    <ul class="articles">
      <li *ngFor="let article of articles" [class.selected]="article === selectedArticle"
            (click)="onSelect(article)">
			  <span class="badge">{{article.id}}</span> {{article.title}}
	  </li>
    </ul>
	<my-article-detail [article]="selectedArticle"></my-article-detail>
`,
	 styles:[`
        .selected {
          background-color: #CFD8DC !important;
          color: white;
        }
        .articles {
          margin: 0 0 2em 0;
          list-style-type: none;
          padding: 0;
          width: 15em;
        }
        .articles li {
          cursor: pointer;
          position: relative;
          left: 0;
          background-color: #EEE;
          margin: .5em;
          padding: .3em 0;
          height: 1.6em;
          border-radius: 4px;
        }
        .articles li.selected:hover {
          background-color: #BBD8DC !important;
          color: white;
        }
        .articles li:hover {
          color: #607D8B;
          background-color: #DDD;
          left: .1em;
        }
        .articles .text {
          position: relative;
          top: -3px;
        }
        .articles .badge {
          display: inline-block;
          font-size: small;
          color: white;
          padding: 0.8em 0.7em 0 0.7em;
          background-color: #607D8B;
          line-height: 1em;
          position: relative;
          left: -1px;
          top: -4px;
          height: 1.8em;
          margin-right: .8em;
          border-radius: 4px 0 0 4px;
        }
      `],
	  providers: [ArticleService]
})
export class AppComponent implements OnInit{ 

	articles:Article[];
    selectedArticle: Article;
	 
	 
	 constructor(private articleService: ArticleService) { }
	 
	   getArticles(): void {
    this.articleService.getArticles().then(articles => this.articles = articles);
  }
  ngOnInit(): void {
    this.getArticles();
  }

     onSelect(article: Article): void { this.selectedArticle = article; }
	
}

	

