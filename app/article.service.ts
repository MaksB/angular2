import { Injectable } from '@angular/core';
import { Article } from './article';
import { ARTICLES } from './mock-article';


@Injectable()
export class ArticleService {
getArticles(): Promise<Article[]> {
	return Promise.resolve(ARTICLES);
	}
}