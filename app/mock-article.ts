import { Article } from './article';

export const ARTICLES: Article[] = [
  { id: 11, title: 'Mr. Nice', text:'Mr. Nice' },
  { id: 12, title: 'Narco', text:'Mr. Nice' },
  { id: 13, title: 'Bombasto', text:'Mr. Nice' },
  { id: 14, title: 'Celeritas', text:'Mr. Nice' },
  { id: 15, title: 'Magneta', text:'Mr. Nice' },
  { id: 16, title: 'RubberMan', text:'Mr. Nice' },
  { id: 17, title: 'Dynama', text:'Mr. Nice' },
  { id: 18, title: 'Dr IQ', text:'Mr. Nice' },
  { id: 19, title: 'Magma', text:'Mr. Nice' },
  { id: 20, title: 'Tornado' , text:'Mr. Nice'}
];